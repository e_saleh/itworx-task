import React, {Component} from "react"

class Welcome extends Component {
    componentDidMount() {
        this.welcomeNode.style.height = window.innerHeight - 120 + "px";
    }

    handleSearch = () => {
        this.props.history.push('/all');
    };

    render() {
        return (
            <div className="col-4 align-items-center flex" ref={(welcomeNode) => this.welcomeNode = welcomeNode}>
                <form className="form-inline flex-direction-col full-width  my-2 my-lg-0">
                    <input className="form-control full-width" type="search" placeholder="Search"
                           aria-label="Search"
                    />
                    <button className="btn btn-outline-success my-4 my-sm-4" type="submit"
                            onClick={this.handleSearch}>Search
                    </button>
                </form>
            </div>
        );
    }
}

export default Welcome