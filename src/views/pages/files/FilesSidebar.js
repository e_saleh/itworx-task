import React, {Component} from "react"
import "../../../css/sidebar/sidebar.scss"
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faSearch} from '@fortawesome/free-solid-svg-icons'
import Calendar from 'react-calendar';

class FilesSidebar extends Component {

    render() {
        return (
            <div className="sidenav">
                <div>
                    <h1>Sort by</h1>
                    <ul>
                        <li>
                            <h5 className="cursor-pointer" onClick={this.props.handleOrderAsc}>Most Recent</h5>
                        </li>
                        <li>
                            <h5 className="cursor-pointer" onClick={this.props.handleOrderDesc}>Oldest First</h5>
                        </li>
                    </ul>
                </div>
                <hr/>
                <div>
                    <h1>Format</h1>
                    <ul>
                        <li>
                            <h5 className="cursor-pointer" onClick={() => this.props.handleTypeFilter('DOCX')}>DOCX</h5>
                        </li>
                        <li>
                            <h5 className="cursor-pointer" onClick={() =>this.props.handleTypeFilter('PDF')}>PDF</h5>
                        </li>
                    </ul>
                    <div className="input-group">
                        <div className="input-group-prepend">
                            <span className="input-group-text cursor-pointer" id="basic-addon1" onClick={this.props.handleSearch}>
                                <FontAwesomeIcon icon={faSearch}/>
                            </span>
                        </div>
                        <input type="text" className="form-control" placeholder="Full Type"
                               name="fileTypeSearch"
                               onChange={this.props.handleSearchInput}/>
                    </div>
                </div>
                <hr/>
                <div>
                    <h1>Date Created</h1>
                    <ul>
                        <li>
                            <h5 className="cursor-pointer" onClick={this.props.handleAnyDate}>Any Time</h5>
                        </li>
                        <li>
                            <h5 className="cursor-pointer" onClick={this.props.handleTodayFilter}>Today</h5>
                        </li>
                        <li>
                            <h5 className="cursor-pointer" onClick={this.props.handleYearFilter}>2019</h5>
                        </li>
                    </ul>
                </div>
                <hr/>
                <Calendar
                    onChange={this.props.handleCalenderChange}
                    value={this.props.date}
                />
            </div>
        );
    }
}

export default FilesSidebar