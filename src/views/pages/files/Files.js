import React, {Component, Fragment} from "react"
import {connect} from "react-redux";
import globalMiddleware from "../../../hocs/globalMiddleware";
import {getTypedResults, getFormattedDateFiles, clearTypedResultsData} from "../../../store/actions/resultActions";
import Card from "../../../components/Card";
import "../../../css/globale.scss"
import {setCardSkeleton} from "../../../helpers/skeleton";
import NoData from "../../../components/NoData";
import FilesSidebar from "./FilesSidebar";
import {isToday, timeFormat} from "../../../helpers/magic";

class Files extends Component {
    state = {
        page: 1,
        limit: 5,
        fileTypeSearch: "",
        noLoadMoreBtn: false,
        ordered: false,
        orderType: "",
        date: new Date(),
    };

    componentDidMount() {
        this.props.clearTypedResultsData();
        this.props.getTypedResults("files", this.state.page, this.state.limit)
    }

    handleLoadMore = () => {
        this.setState({
            page: this.state.page + 1,
        }, () => {
            if (this.state.ordered) {
                this.props.getTypedResults("files", this.state.page, this.state.limit, this.state.orderType, null, null, "created");
            } else {
                this.props.getTypedResults("files", this.state.page, this.state.limit)
            }
        })
    };
    handleOrderAsc = async () => {
        this.setState({
            noLoadMoreBtn: false,
            page: 1,
            limit: 5,
            ordered: true,
            orderType: "asc",
        });
        await this.props.clearTypedResultsData();
        this.props.getTypedResults("files", this.state.page, this.state.limit, "asc", null, null, "created");
    };
    handleOrderDesc = async () => {
        this.setState({
            noLoadMoreBtn: false,
            page: 1,
            limit: 5,
            ordered: true,
            orderType: "desc",
        });
        await this.props.clearTypedResultsData();
        this.props.getTypedResults("files", this.state.page, this.state.limit, "desc", null, null, "created")
    };
    handleTypeFilter = async (filter_value) => {
        this.setState({
            noLoadMoreBtn: true,
        });
        await this.props.clearTypedResultsData();
        this.props.getTypedResults("files", null, null, null, "appType", filter_value)
    };
    handleAnyDate = async () => {
        this.setState({
            noLoadMoreBtn: false,
        });
        await this.props.clearTypedResultsData();
        this.props.getTypedResults("files", this.state.page, this.state.limit)
    };
    handleTodayFilter = async () => {
        this.setState({
            noLoadMoreBtn: true,
        });
        await this.props.clearTypedResultsData();
       await this.props.getFormattedDateFiles("files", "today");
    };
    handleYearFilter = async () => {
        this.setState({
            noLoadMoreBtn: true,
        });
        await this.props.clearTypedResultsData();
        await this.props.getFormattedDateFiles("files", "year", "2019");
    };
    handleSearchInput = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    };
    handleSearch = async () => {

        await this.props.clearTypedResultsData();
        if (this.state.fileTypeSearch) {
            this.setState({
                noLoadMoreBtn: true
            });
            this.props.getTypedResults("files", null, null, null, "appType", this.state.fileTypeSearch)
        } else {
            this.setState({
                noLoadMoreBtn: false
            });
            this.props.getTypedResults("files", this.state.page, this.state.limit)
        }
    };
    handleCalenderChange = (date) => {
        this.setState({
            date: date,
            noLoadMoreBtn: true,
        }, () => {
             this.props.clearTypedResultsData();
            this.props.getFormattedDateFiles("files", "date", date)
        });
    };

    render() {
        const {typedResults, lastPage, typedResultsIsLoading} = this.props;

        const filesResultsCards = typedResults && typedResults.length ? typedResults.map((file, index) => {


            return (
                <Fragment key={index}>
                    <Card isAll={false} image={file.image} title={file.name}
                          subTitle={file.appType}
                          info={timeFormat(file.created)}/>
                </Fragment>
            )
        }) : <NoData/>;
        return (
            <Fragment>
                <FilesSidebar handleOrderAsc={this.handleOrderAsc}
                              handleOrderDesc={this.handleOrderDesc}
                              handleSearchInput={this.handleSearchInput}
                              handleSearch={this.handleSearch}
                              handleAnyDate={this.handleAnyDate}
                              handleTodayFilter={this.handleTodayFilter}
                              handleYearFilter={this.handleYearFilter}
                              date={this.state.date}
                              handleCalenderChange={this.handleCalenderChange}
                              handleTypeFilter={(filter_value) => this.handleTypeFilter(filter_value)}/>
                {!typedResultsIsLoading
                    ? <div className="results-container">
                        <div className="results">
                            {filesResultsCards}
                        </div>
                        {(lastPage != this.state.page && typedResults && typedResults.length) ?
                            <button className="btn" hidden={this.state.noLoadMoreBtn} onClick={this.handleLoadMore}>Load
                                more
                            </button> : null}
                    </div>
                    : setCardSkeleton(5)
                }

            </Fragment>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        typedResults: state.resultReducer.typedResults,
        typedResultsIsLoading: state.resultReducer.typedResultsIsLoading,
        typedResultsIsEmpty: state.resultReducer.typedResultsIsEmpty,
        lastPage: state.resultReducer.lastPage,
    }
};


const mapDispatchToProps = (dispatch) => {
    return {
        getTypedResults: (type, page, limit, order, filter_name, filter_value, sort) => dispatch(getTypedResults(type, page, limit, order, filter_name, filter_value, sort)),
        getFormattedDateFiles: (type,dateType,date) => dispatch(getFormattedDateFiles(type,dateType, date)),
        clearTypedResultsData: () => dispatch(clearTypedResultsData())
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(globalMiddleware(Files))