import React, {Component} from "react"
import "./../../css/login/login.scss"
import {usersApiLink} from "../../config";
import axios from "axios";
import {showToast} from "../../helpers/magic";

class Login extends Component {
    state = {
        username: "",
        password: "",
        loading: false
    };

    componentDidMount() {
        this.loginNode.style.height = window.innerHeight - 120 + "px";
    }

    handleInputChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        });
    };
    handleLogin = (e) => {
        // start loading
        this.setState({
            loading: true,
        });
        // prevent default behavior
        e.preventDefault();


        axios.get(usersApiLink + "?username=" + this.state.username + "&password=" + this.state.password).then(res => {
            // stop loading
            this.setState({
                loading: false,
            });
            if (res.data && res.data.length) {
                this.setState({
                    username: "",
                    password: "",
                });
                res.data.map(user => {
                    if (user.permissions.viewDashboard){
                        showToast("You are login successfully", "green");
                        localStorage.setItem("isLogin", "true");
                        this.props.history.push("/dashboard");
                    }else {
                        this.props.history.push("/");
                        showToast("You are not authorized", "red");
                    }
                });
            }else {
                showToast("The username or password is incorrect", "red");
            }
        }).catch(error => {
            showToast("Something went wrong, please try again", "red");
            this.setState({
                loading: true,
            });
        });
    };

    render() {
        return (
            <div className="col-4 align-items-center flex" ref={(loginNode) => this.loginNode = loginNode}>
                <div className="full-width">
                    <h1 className="text-center login-title capitalized">Welcome Back</h1>
                    <div className="account-wall">
                        <img className="profile-img"
                             src="https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=120"
                             alt=""/>
                        <form className="form-signin" onSubmit={this.handleLogin}>
                            <input type="text" className="form-control" name="username" placeholder="Username"
                                   required
                                   onChange={this.handleInputChange}/>
                            <input type="password" className="form-control" placeholder="Password" name="password"
                                   required
                                   onChange={this.handleInputChange}/>
                            <button className="btn btn-lg btn-primary btn-block" type="submit">
                                Sign in
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default Login