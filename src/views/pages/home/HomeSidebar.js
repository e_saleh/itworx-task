import React, {Component} from "react"
import "../../../css/sidebar/sidebar.scss"
import Calendar from 'react-calendar';

class HomeSidebar extends Component {

    render() {
        return (
            <div className="sidenav">
                <div>
                    <h1>Sort by</h1>
                    <ul>
                        <li>
                            <h5 className="cursor-pointer" onClick={this.props.handleOrderAsc}>Most Recent</h5>
                        </li>
                        <li>
                            <h5 className="cursor-pointer" onClick={this.props.handleOrderDesc}>Oldest First</h5>
                        </li>
                    </ul>
                </div>
                <hr/>
                <div>
                    <h1>Date Created</h1>
                    <ul>
                        <li>
                            <h5 className="cursor-pointer" onClick={this.props.handleAnyDate}>Any Time</h5>
                        </li>
                        <li>
                            <h5 className="cursor-pointer" onClick={this.props.handleTodayFilter}>Today</h5>
                        </li>
                        <li>
                            <h5 className="cursor-pointer" onClick={this.props.handleYearFilter}>2019</h5>
                        </li>
                    </ul>
                </div>
                <hr/>
                <Calendar
                    onChange={this.props.handleCalenderChange}
                    value={this.props.date}
                />
            </div>
        );
    }
}

export default HomeSidebar