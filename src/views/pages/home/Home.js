import React, {Component, Fragment} from "react"
import {connect} from "react-redux";
import globalMiddleware from "../../../hocs/globalMiddleware";
import {
    getHomeData,
    clearResultsData,
    getTypedResults,
    clearTypedResultsData, getFormattedDateFiles
} from "../../../store/actions/resultActions";
import "../../../css/globale.scss"
import {setHomeSkeleton} from "../../../helpers/skeleton";
import HomeSidebar from "./HomeSidebar";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faFile} from '@fortawesome/free-solid-svg-icons'
import {faAd} from '@fortawesome/free-solid-svg-icons'
import {faUser} from '@fortawesome/free-solid-svg-icons'
import {faSearch} from '@fortawesome/free-solid-svg-icons'
import ProgressiveImage from "react-progressive-image-loading";
import {Link} from "react-router-dom"

class Home extends Component {
    state = {
        date: new Date()
    };

    async componentDidMount() {
        await Promise.all([
            this.props.clearTypedResultsData(),
            this.props.clearResultsData(),
            this.props.getHomeData("people", 1, 3),
            this.props.getHomeData("apps", 1, 3),
            this.props.getHomeData("files", 1, 3),
        ])
    }

    handleOrderAsc = async () => {
        await this.props.clearTypedResultsData();
        this.props.getTypedResults("files", 1, 3, "asc")
    };
    handleOrderDesc = async () => {
        await this.props.clearTypedResultsData();
        this.props.getTypedResults("files", 1, 3, "desc")
    };
    handleAnyDate = async () => {
        await this.props.clearTypedResultsData();
        this.props.getTypedResults("files", this.state.page, this.state.limit)
    };
    handleTodayFilter = async () => {
        await this.props.clearTypedResultsData();
        await this.props.getFormattedDateFiles("files", "today");
    };
    handleYearFilter = async () => {
        await this.props.clearTypedResultsData();
        await this.props.getFormattedDateFiles("files", "year", "2019");
    };
    handleCalenderChange = (date) => {
        this.setState({
            date: date,
        }, () => {
            this.props.clearTypedResultsData();
            this.props.getFormattedDateFiles("files", "date", date)
        });
    };

    render() {
        const {typedResults, homeResultsIsLoading,filesResults, appsResults, peopleResults, lastPage, typedResultsIsLoading} = this.props;

        const appsResultsCards = appsResults && appsResults.length ? appsResults.map((app, index) => {
            return (
                <Fragment key={index}>
                    <div className="flex-m">
                        {/*<img src={app.image} alt="avatar" className="img-avatar"/>*/}
                        <ProgressiveImage
                            preview="images/grey.jpg"
                            src={app.image}
                            render={(src, style) => <img alt="avatar" src={src} className="img-avatar" style={style}/>}
                        />
                        <div className="flex-direction-col ml-20 card-text">
                            <h4 className="no-margin">{app.name}</h4>
                            <p className="no-margin grey">{app.appType}</p>
                        </div>
                    </div>
                </Fragment>
            )
        }) : null;
        const filesResultsCards = filesResults && filesResults.length ? filesResults.map((file, index) => {
            return (
                <Fragment key={index}>
                    <div className="flex-m">
                        <ProgressiveImage
                            preview="images/grey.jpg"
                            src={file.image}
                            render={(src, style) => <img alt="avatar" src={src} className="img-avatar" style={style}/>}
                        />
                        <div className="flex-direction-col ml-20 card-text">
                            <h4 className="no-margin">{file.name}</h4>
                            <p className="no-margin grey">{file.appType}</p>
                            <p className="no-margin">{file.created}</p>
                        </div>
                    </div>
                </Fragment>
            )
        }) : null;
        const peopleResultsCards = peopleResults && peopleResults.length ? peopleResults.map((item, index) => {
            return (
                <Fragment key={index}>
                    <div className="flex-m">
                        <ProgressiveImage
                            preview="images/grey.jpg"
                            src={item.image}
                            render={(src, style) => <img alt="avatar" src={src} className="img-avatar" style={style}/>}
                        />
                        <div className="flex-direction-col ml-20 card-text">
                            <h4 className="no-margin">{item.name}</h4>
                            <p className="no-margin grey">{item.jobTitle}</p>
                            <p className="no-margin">{item.department}</p>
                        </div>
                    </div>
                </Fragment>
            )
        }) : null;

        return (
            <Fragment>
                <HomeSidebar handleOrderAsc={this.handleOrderAsc}
                             handleOrderDesc={this.handleOrderDesc}
                             handleTodayFilter={this.handleTodayFilter}
                             handleYearFilter={this.handleYearFilter}
                             date={this.state.date}
                             handleCalenderChange={this.handleCalenderChange}
                             handleTypeFilter={(filter_value) => this.handleTypeFilter(filter_value)}
                />
                {!homeResultsIsLoading
                    ? <div className="results-container">
                        <div className="home-results">
                            {peopleResultsCards ?
                                <div className="card-area">
                                    <div className="card gray no-padding">
                                        <div className="card-body flex-col justify-content-center">
                                            <div className="flex">
                                                <FontAwesomeIcon icon={faUser} size="2x"/>
                                                <h3 className="ml-20 capitalized ">People</h3>
                                            </div>
                                            {peopleResultsCards}
                                        </div>
                                        <div className="calendar-poster flex-c-m">
                                            <FontAwesomeIcon icon={faSearch} color="grey"/>
                                            <Link to="/people" className="no-margin grey capitalized">See all people
                                                Results</Link>
                                        </div>
                                    </div>
                                </div> : null}
                            {filesResultsCards ?
                                <div className="card-area">
                                    <div className="card gray no-padding">
                                        <div className="card-body flex-col justify-content-center">
                                            <div className="flex">
                                                <FontAwesomeIcon icon={faFile} size="2x"/>
                                                <h3 className="ml-20 capitalized ">Files</h3>
                                            </div>
                                            {filesResultsCards}
                                        </div>
                                        <div className="calendar-poster flex-c-m">
                                            <FontAwesomeIcon icon={faSearch} color="grey"/>
                                            <Link to="/files" className="no-margin grey capitalized">See all Files
                                                Results</Link>
                                        </div>
                                    </div>
                                </div>
                                : null}
                            {appsResultsCards ?
                                <div className="card-area">
                                    <div className="card gray no-padding">
                                        <div className="card-body flex-col justify-content-center">
                                            <div className="flex">
                                                <FontAwesomeIcon icon={faAd} size="2x"/>
                                                <h3 className="ml-20 capitalized ">Apps</h3>
                                            </div>
                                            {appsResultsCards}
                                        </div>
                                        <div className="calendar-poster flex-c-m">
                                            <FontAwesomeIcon icon={faSearch} color="grey"/>
                                            <Link to="/apps" className="no-margin grey capitalized">See all apps
                                                Results</Link>
                                        </div>
                                    </div>
                                </div>
                                : null}
                        </div>
                    </div>

                    : setHomeSkeleton(3)
                }

            </Fragment>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        filesResults: state.resultReducer.filesResults,
        peopleResults: state.resultReducer.peopleResults,
        appsResults: state.resultReducer.appsResults,
        typedResultsIsLoading: state.resultReducer.typedResultsIsLoading,
        homeResultsIsLoading: state.resultReducer.homeResultsIsLoading,
        typedResultsIsEmpty: state.resultReducer.typedResultsIsEmpty,
        lastPage: state.resultReducer.lastPage,
    }
};


const mapDispatchToProps = (dispatch) => {
    return {
        getHomeData: (type, page, limit) => dispatch(getHomeData(type, page, limit)),
        clearResultsData: () => dispatch(clearResultsData()),
        getTypedResults: (type, page, limit, order, filter_name, filter_value, sort) => dispatch(getTypedResults(type, page, limit, order, filter_name, filter_value, sort)),
        getFormattedDateFiles: (type, dateType, date) => dispatch(getFormattedDateFiles(type, dateType, date)),
        clearTypedResultsData: () => dispatch(clearTypedResultsData())
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(globalMiddleware(Home))