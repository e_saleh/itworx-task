import React, {Component, Fragment} from "react"
import {connect} from "react-redux";
import globalMiddleware from "../../../hocs/globalMiddleware";
import {getTypedResults, clearTypedResultsData} from "../../../store/actions/resultActions";
import Card from "../../../components/Card";
import "../../../css/globale.scss"
import {setCardSkeleton} from "../../../helpers/skeleton";
import NoData from "../../../components/NoData";
import PeopleSidebar from "./PeopleSidebar";

class People extends Component {
    state = {
        page: 1,
        limit: 5,
        jobTitleSearch: "",
        departmentSearch: "",
        noLoadMoreBtn: false,
    };

    componentDidMount() {
        this.props.clearTypedResultsData();
        this.props.getTypedResults("people", this.state.page, this.state.limit)
    }

    handleLoadMore = () => {
        this.setState({
            page: this.state.page + 1,
        }, () => {
            this.props.getTypedResults("people", this.state.page, this.state.limit)
        })
    };
    handleOrderAsc = async () => {
        this.setState({
            noLoadMoreBtn: false,
            page: 1,
            limit: 5,
        });
        await this.props.clearTypedResultsData();
        this.props.getTypedResults("people", this.state.page, this.state.limit, "asc")
    };
    handleOrderDesc = async () => {
        this.setState({
            noLoadMoreBtn: false,
            page: 1,
            limit: 5,
        });
        await this.props.clearTypedResultsData();
        this.props.getTypedResults("people", this.state.page, this.state.limit, "desc")
    };
    handleAnyFilter = async () => {
        this.setState({
            noLoadMoreBtn: false,
            page: 1,
            limit: 5,
        });
        await this.props.clearTypedResultsData();
        this.props.getTypedResults("people")
    };
    handleExecutiveFilter = async () => {
        this.setState({
            noLoadMoreBtn: false,
            page: 1,
            limit: 5,
        });
        await this.props.clearTypedResultsData();
        this.props.getTypedResults("people", null, null, null, "jobTitle", "Executive")
    };
    handleHrFilter = async () => {
        this.setState({
            noLoadMoreBtn: false,
            page: 1,
            limit: 5,
        });
        await this.props.clearTypedResultsData();
        this.props.getTypedResults("people", this.state.page, this.state.limit, null, "department", "hr")
    };
    handleSearchInput = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    };
    handleSearch = async (filterName) => {
       await this.props.clearTypedResultsData();

        switch (filterName) {
            case "jobTitle":
                if (this.state.jobTitleSearch) {
                    this.setState({
                        noLoadMoreBtn: true
                    });
                    this.props.getTypedResults("people", null, null, null, filterName, this.state.jobTitleSearch)
                } else {
                    this.setState({
                        noLoadMoreBtn: false
                    });
                    this.props.getTypedResults("people", this.state.page, this.state.limit)
                }
                break;
            case "department":
                if (this.state.departmentSearch) {
                    this.setState({
                        noLoadMoreBtn: true
                    });
                    this.props.getTypedResults("people", null, null, null, filterName, this.state.departmentSearch)
                } else {
                    this.setState({
                        noLoadMoreBtn: false
                    });
                    this.props.getTypedResults("people", this.state.page, this.state.limit)
                }
                break;
            default:
            //nothing
        }

    };

    render() {
        const {typedResults, lastPage, typedResultsIsLoading} = this.props;

        const peopleResultsCards = typedResults && typedResults.length ? typedResults.map((peopleItem, index) => {
            return (
                <Fragment key={index}>
                    <Card isAll={false} image={peopleItem.image} title={peopleItem.name}
                          subTitle={peopleItem.jobTitle}
                          info={peopleItem.department}/>
                </Fragment>
            )
        }) : <NoData/>;
        return (
            <Fragment>
                <PeopleSidebar handleOrderAsc={this.handleOrderAsc}
                               handleOrderDesc={this.handleOrderDesc}
                               handleAnyFilter={this.handleAnyFilter}
                               handleExecutiveFilter={this.handleExecutiveFilter}
                               handleHrFilter={this.handleHrFilter}
                               handleSearchInput={this.handleSearchInput}
                               handleSearch={(filterName) => this.handleSearch(filterName)}/>
                {!typedResultsIsLoading
                    ? <div className="results-container">
                        <div className="results">
                            {peopleResultsCards}
                        </div>
                        {(lastPage != this.state.page && typedResults && typedResults.length) ?
                            <button className="btn" hidden={this.state.noLoadMoreBtn} onClick={this.handleLoadMore}>Load more
                            </button> : null}
                    </div>
                    : setCardSkeleton(5)
                }

            </Fragment>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        typedResults: state.resultReducer.typedResults,
        typedResultsIsLoading: state.resultReducer.typedResultsIsLoading,
        typedResultsIsEmpty: state.resultReducer.typedResultsIsEmpty,
        lastPage: state.resultReducer.lastPage,
    }
};


const mapDispatchToProps = (dispatch) => {
    return {
        getTypedResults: (type, page, limit, order, filter_name, filter_value) => dispatch(getTypedResults(type, page, limit, order, filter_name, filter_value)),
        clearTypedResultsData: () => dispatch(clearTypedResultsData())
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(globalMiddleware(People))