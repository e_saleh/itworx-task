import React, {Component} from "react"
import "../../../css/sidebar/sidebar.scss"
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faSearch} from '@fortawesome/free-solid-svg-icons'

class PeopleSidebar extends Component {

    render() {
        return (
            <div className="sidenav">
                <div>
                    <h1>Sort by</h1>
                    <ul>
                        <li>
                            <h5 className="cursor-pointer" onClick={this.props.handleOrderAsc}>Most Recent</h5>
                        </li>
                        <li>
                            <h5 className="cursor-pointer" onClick={this.props.handleOrderDesc}>Oldest First</h5>
                        </li>
                    </ul>
                </div>
                <hr/>
                <div>
                    <h1>Job Title</h1>
                    <ul>
                        <li>
                            <h5 className="cursor-pointer" onClick={this.props.handleAnyFilter}>Any</h5>
                        </li>
                        <li>
                            <h5 className="cursor-pointer" onClick={this.props.handleExecutiveFilter}>Executive</h5>
                        </li>
                    </ul>
                    <div className="input-group">
                        <div className="input-group-prepend">
                            <span className="input-group-text cursor-pointer" id="basic-addon1" onClick={() => this.props.handleSearch("jobTitle")}>
                                <FontAwesomeIcon icon={faSearch}/>
                            </span>
                        </div>
                        <input type="text" className="form-control" placeholder="Full Job title"
                               name="jobTitleSearch"
                               onChange={this.props.handleSearchInput}/>
                    </div>
                </div>
                <hr/>
                <div>
                    <h1>Department</h1>
                    <ul>
                        <li>
                            <h5 className="cursor-pointer" onClick={this.props.handleAnyFilter}>Any</h5>
                        </li>
                        <li>
                            <h5 className="cursor-pointer" onClick={this.props.handleHrFilter}>HR</h5>
                        </li>
                    </ul>
                    <div className="input-group">
                        <div className="input-group-prepend">
                            <span className="input-group-text cursor-pointer" id="basic-addon1" onClick={() => this.props.handleSearch("department")}>
                                <FontAwesomeIcon icon={faSearch}/>
                            </span>
                        </div>
                        <input type="text" className="form-control" placeholder="Full Department"
                               name="departmentSearch"
                               onChange={this.props.handleSearchInput}/>
                    </div>
                </div>
            </div>
        );
    }
}

export default PeopleSidebar