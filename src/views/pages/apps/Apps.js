import React, {Component, Fragment} from "react"
import {connect} from "react-redux";
import globalMiddleware from "../../../hocs/globalMiddleware";
import {getTypedResults, clearTypedResultsData} from "../../../store/actions/resultActions";
import Card from "../../../components/Card";
import "../../../css/globale.scss"
import {setCardSkeleton} from "../../../helpers/skeleton";
import NoData from "../../../components/NoData";
import AppsSidebar from "./AppsSidebar";

class Apps extends Component {
    state = {
        page: 1,
        limit: 5,
        noLoadMoreBtn: false,
    };

    componentDidMount() {
        this.props.clearTypedResultsData();
        this.props.getTypedResults("apps", this.state.page, this.state.limit)
    }

    handleLoadMore = () => {
        this.setState({
            page: this.state.page + 1,
        }, () => {
            this.props.getTypedResults("apps", this.state.page, this.state.limit)
        })
    };
    handleOrderAsc = async () => {
        this.setState({
            noLoadMoreBtn: false,
            page: 1,
            limit: 5,
        });
        await this.props.clearTypedResultsData();
        this.props.getTypedResults("apps", this.state.page, this.state.limit, "asc")
    };
    handleOrderDesc = async () => {
        this.setState({
            noLoadMoreBtn: false,
            page: 1,
            limit: 5,
        });
        await this.props.clearTypedResultsData();
        this.props.getTypedResults("apps", this.state.page, this.state.limit, "desc")
    };
    handleAnyFilter = async () => {
        this.setState({
            noLoadMoreBtn: false,
            page: 1,
            limit: 5,
        });
        await this.props.clearTypedResultsData();
        this.props.getTypedResults("apps", this.state.page, this.state.limit)
    };
    handleFilter = async (filter_value) => {
        this.setState({
            noLoadMoreBtn: true
        });
        await this.props.clearTypedResultsData();
        this.props.getTypedResults("apps", null, null, null, "appType", filter_value)
    };
    render() {
        const {typedResults, lastPage, typedResultsIsLoading} = this.props;

        const appsResultsCards = typedResults && typedResults.length ? typedResults.map((app, index) => {
            return (
                <Fragment key={index}>
                    <Card isAll={false} image={app.image} title={app.name}
                          subTitle={app.appType}/>
                </Fragment>
            )
        }) : <NoData/>;
        return (
            <Fragment>
                <AppsSidebar handleOrderAsc={this.handleOrderAsc}
                             handleOrderDesc={this.handleOrderDesc}
                             handleAnyFilter={this.handleAnyFilter}
                             handleFilter={(filter_value) => this.handleFilter(filter_value)}/>
                {!typedResultsIsLoading
                    ? <div className="results-container">
                        <div className="results">
                            {appsResultsCards}
                        </div>
                        {(lastPage != this.state.page && typedResults && typedResults.length) ?
                            <button className="btn" hidden={this.state.noLoadMoreBtn} onClick={this.handleLoadMore}>
                                Load more
                            </button> : null}
                    </div>
                    : setCardSkeleton(5)
                }

            </Fragment>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        typedResults: state.resultReducer.typedResults,
        typedResultsIsLoading: state.resultReducer.typedResultsIsLoading,
        typedResultsIsEmpty: state.resultReducer.typedResultsIsEmpty,
        lastPage: state.resultReducer.lastPage,
    }
};


const mapDispatchToProps = (dispatch) => {
    return {
        getTypedResults: (type, page, limit, order, filter_name, filter_value) => dispatch(getTypedResults(type, page, limit, order, filter_name, filter_value)),
        clearTypedResultsData: () => dispatch(clearTypedResultsData())
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(globalMiddleware(Apps))