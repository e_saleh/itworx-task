import React, {Component} from "react"
import "../../../css/sidebar/sidebar.scss"

class AppsSidebar extends Component {

    render() {
        return (
            <div className="sidenav">
                <div>
                    <h1>Sort by</h1>
                    <ul>
                        <li>
                            <h5 className="cursor-pointer" onClick={this.props.handleOrderAsc}>Most Recent</h5>
                        </li>
                        <li>
                            <h5 className="cursor-pointer" onClick={this.props.handleOrderDesc}>Oldest First</h5>
                        </li>
                    </ul>
                </div>
                <hr/>
                <div>
                    <h1>Type</h1>
                    <ul>
                        <li>
                            <h5 className="cursor-pointer" onClick={this.props.handleAnyFilter}>Any</h5>
                        </li>
                        <li>
                            <h5 className="cursor-pointer" onClick={() => this.props.handleFilter("Internal")}>Internal</h5>
                        </li>
                        <li>
                            <h5 className="cursor-pointer" onClick={() => this.props.handleFilter("External")}>External</h5>
                        </li>
                    </ul>
                </div>

            </div>
        );
    }
}

export default AppsSidebar