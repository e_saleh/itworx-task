import React, {Component} from "react"
import {getFeedback} from "../../store/actions/dashboardActions";
import {connect} from "react-redux";
import globalMiddleware from "../../hocs/globalMiddleware";
import PieChart from 'react-minimal-pie-chart';
import NoData from "../../components/NoData";

class Dashboard extends Component {
    constructor(props) {
        super(props);
        this.isLogin = localStorage.getItem("isLogin") ? JSON.parse(localStorage.getItem("isLogin")) : null;
    }

    componentDidMount() {
        if (!this.isLogin) {
            this.props.history.push('/login')
        }
        this.props.getFeedback();
    }

    render() {
        const {feedbackResults, noOfFeedbackYes, noOfFeedbackNo} = this.props;
        const feedback = feedbackResults && feedbackResults.length ?
            feedbackResults.map(item => {
                return (
                    item.feedback ? <p>feedback: {item.feedback}</p> : null
                )
            })
            : <NoData/>
        const data = [
            {title: "YES", value: noOfFeedbackYes, color: "#2f7d6d"},
            {title: "No", value: noOfFeedbackNo, color: "#3da18d"},
        ]
        return (
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-md-4">
                        <PieChart
                            data={data}
                        />
                    </div>
                </div>
                <div className="row justify-content-center">
                    <div className="col-md-6">
                        <div className="card-area mt-50" style={{width: "100%"}}>
                            <div className="card gray no-padding">
                                <div className="card-body flex-col justify-content-center">
                                    {feedback}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        feedbackResults: state.dashboardReducer.feedbackResults,
        noOfFeedbackYes: state.dashboardReducer.noOfFeedbackYes,
        noOfFeedbackNo: state.dashboardReducer.noOfFeedbackNo
    }
};


const mapDispatchToProps = (dispatch) => {
    return {
        getFeedback: () => dispatch(getFeedback())
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(globalMiddleware(Dashboard))