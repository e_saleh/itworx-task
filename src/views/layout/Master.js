import React, {Component} from "react";
import Navigator from "../../components/layout/Navigator";
import Footer from "../../components/layout/Footer";
import "./../../css/basics.scss";
import "./../../css/home/home.scss"
class Master extends Component {

    render() {
        const children = React.Children.map(this.props.children, (child, index) => {
            return React.cloneElement(child, {
            });
        });

        return (

            <div className="default-layout">
                <Navigator isSelect={this.props.isSelect}/>
                <div className="container-fluid mt-40 mb-80">
                    <div className="row justify-content-center">
                        {children}
                    </div>
                </div>
                <Footer/>
            </div>
        )
    }
}

export default Master
