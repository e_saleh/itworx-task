import {Bone, Skeleton, Skull} from "react-loading-skeleton-placeholders";
import React from "react";

export const setDoctorsSkeleton = (count) => {
    let items = [];
    for (let i = 0; i < count; i++) {
        items.push(
            <div className="doctors-skeleton col-md-6 col-xs-12" key={i}>
                <Skull height={10} width={70}/>
                <Skeleton amount={4}/>
                <div className="row text-center bone-center" style={{display: "flex", justifyContent: "space-around"}}>
                    <Bone style={{margin: 'auto'}} height={10} width={20}/>
                    <Bone style={{margin: 'auto'}} height={10} width={20}/>
                    <Bone style={{margin: 'auto'}} height={30} width={20}/>
                </div>
            </div>
        )
    }
    return items;
};

export const setBlogCardSkeleton = (count, noCol) => {
    let items = [];
    for (let i = 0; i < count; i++) {
        items.push(
            <div className={"col-sm-12 col-md-" + noCol} key={i}>
                <div className="">
                    <div className="full-width full-height">
                        <Bone height={209}/>
                    </div>
                    <div className="full-width full-height">

                        <Skeleton amount={4}/>
                    </div>
                </div>
            </div>
        )
    }
    return items;
};

export const setOffersSkeleton = (count) => {
    let items = [];
    for (let i = 0; i < count; i++) {
        items.push(
            <div className="col-md-6 col-sm-6 col-xs-12" key={i}>
                <div style={{marginBottom: "15px"}}>
                    <Bone style={{margin: 'auto'}} height={300}/>
                </div>
            </div>
        )
    }
    return items;
};
export const setOfferDetailsSkeleton = () => {
    return (
        <div className="offer-details-box">
            <div className="offer-image">
                <Bone width={100} height={256}/>
            </div>

            {/*   <div className="offer-doc-details">
                <Skull className="offer-avatar pull-left"/>
                <div className="ml-20" style={{width: "100%"}}>
                    <Bone width={50}/>
                    <Bone width={20}/>
                </div>
            </div>*/}

            {/*     <div className="offer-info">
                <div>
                    <div className="offer-info-name">
                        <Bone className="offer-name mb-0" width={30}/>
                    </div>
                    <div className="offer-booked-details">
                        <div className="offer-booked">
                            <Bone width={30}/>
                            <Bone width={30}/>
                        </div>
                        <div className="offer-discount">
                            <Bone width={100}/>
                        </div>
                    </div>
                </div>
            </div>*/}

            <div className="offer-info">
                <Bone width={100}/>
                <Bone width={60}/>
                <Bone width={30}/>
                <Skeleton amount={4}/>
                <div className="offer-option">
                    <div className="flex-direction-row flex share-area-skeleton">
                        <Skull width={20} height={20}/>
                        <Skull width={20} height={20}/>
                        <Skull width={20} height={20}/>
                        <Skull width={20} height={20}/>
                    </div>
                </div>


            </div>
        </div>
    )
}
export const setSmallBoneSkeleton = (count) => {
    let items = [];
    for (let i = 0; i < count; i++) {
        items.push(
            <Bone width={50} key={i}/>
        )
    }

    return items;
};

export const setSinglePostSkeleton = (count) => {
    let items = [];
    for (let i = 0; i < count; i++) {
        items.push(
            <div className="col-md-8 ftco-animate" key={i}>
                <div className="row row-50">
                    <div className="col-lg-12">
                        <article className="post-creative">
                            <h3 className="post-creative-title"><Bone width={60} height={20}/></h3>
                            <div className="date-title">
                                <Bone width={20} height={15} className="pull-left"/>
                                <Bone width={20} height={15} className="pull-left"/>
                            </div>

                            <br/>

                            <Bone width={100} height={400}/>

                            <br/>

                            <div><Skeleton amount={10}/></div>
                        </article>
                        <div className="section-sm mt-50">
                            <h3 className="mb-15"><Bone width={17} height={20}/></h3>
                            <div className="comment-classic-group mb-30">
                                <article className="comment-classic">
                                    <Skull/>
                                    <div className="comment-classic-main">
                                        <div className="comment-classic-name"><Bone width={17} height={20}/>
                                        </div>
                                        <div className="comment-classic-text">
                                            <div><Skeleton amount={3}/></div>
                                        </div>
                                        <Bone width={22} height={10}/>
                                    </div>
                                </article>
                            </div>
                            <div className="comment-classic-group mb-30">
                                <article className="comment-classic">
                                    <Skull/>

                                    <div className="comment-classic-main">
                                        <div className="comment-classic-name"><Bone width={17} height={20}/>
                                        </div>
                                        <div className="comment-classic-text">
                                            <div><Skeleton amount={3}/></div>
                                        </div>

                                        <Bone width={22} height={10}/>

                                    </div>
                                </article>
                            </div>
                            <div className="comment-classic-group mb-30">
                                <article className="comment-classic">
                                    <Skull/>

                                    <div className="comment-classic-main">
                                        <div className="comment-classic-name"><Bone width={17} height={20}/>
                                        </div>
                                        <div className="comment-classic-text">
                                            <div><Skeleton amount={3}/></div>
                                        </div>
                                        <Bone width={22} height={10}/>
                                    </div>
                                </article>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
    return items;
};

export const setProfileSkeleton = () => {
    return (
        <div className="row">
            <div className="col-md-8 col-sm-12 col-xs-12">
                <div>
                    <Skeleton skull={true} amount={3}/>
                </div>
                <div className="mt-50 mb-50 ">
                    <div>
                        <Bone width={100} height={150}/>
                        <Bone width={100} height={150}/>
                        <Bone width={100} height={150}/>
                    </div>
                </div>
            </div>
            <div className="col-md-4 col-sm-12 col-xs-12" id="profile-sidebar-skeleton">
                <div className="profile-reservations-card mt-45">
                    <div className="side-bar-branch">
                        <div className="branch">
                            <div className="branch-header text-center">
                                <Bone width={70}/>
                            </div>
                        </div>

                        <div className="row margin-top-30">
                            <div className="col-4 offset-4 text-center">
                                <Skull/>
                            </div>
                        </div>
                    </div>


                    {/*
                    <div className="margin-top-30">
                        <div className="branch">
                            <div className="branch-header text-center">
                                <Bone width={70}/>
                            </div>
                            <div className="p-20">
                                <Bone width={50}/>
                                <Bone width={30}/>
                                <div className="row margin-top-30">
                                    <div className="col-4 offset-4 text-center">
                                        <Skull/>
                                    </div>
                                </div>
                                <Skeleton amount={3}/>
                            </div>
                        </div>
                    </div>
*/}
                </div>
            </div>
        </div>

    )
};

export const setDoctorProfileCardSkeleton = () => {
    return (
        <div className="row">
            <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div>
                    <Skeleton skull={true} amount={3}/>
                </div>
            </div>
        </div>

    )
};
export const setDoctorProfileSkeleton = () => {
    return (
        <div className="row">
            <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div className="mt-50 mb-50 ">
                    <div>
                        <Bone width={30}/>
                    </div>
                    <Skeleton amount={3}/>
                </div>
            </div>
        </div>

    )
};

export const setBranchSkeleton = () => {
    return (
        <div className="doctors-details-card">
            <div className="sidebar-doctor-details shadow-4d">
                <div className="branch">
                    <div className="branch-header branch-header-skeleton">
                        <Bone width={80}/>
                    </div>

                    <div className="branch-details">
                        <div className="branch-info">
                            <div>
                                <Bone width={60}/>
                            </div>
                            <div>
                                <Bone width={60}/>

                            </div>
                            <div>
                                <Bone width={60}/>
                            </div>
                        </div>
                        <div>
                            <Bone width={100} height={30}/>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    )
};

export const setFeesSkeleton = () => {
    return (
        <div>
            <ul className="list-unstyled">

                <li>
                    <span><Bone width={30}/></span>
                    <span className="pull-right"><Bone width={30}/></span>
                </li>


                <li>
                    <span><Bone width={30}/></span>
                    <span className="pull-right"><Bone width={30}/></span>
                </li>

                <li>
                    <span><Bone width={30}/></span>
                    <span className="pull-right"><Bone width={30}/></span>
                </li>

                <hr/>

                <li>
                    <span>Total</span>
                    <span
                        className="pull-right"></span>
                </li>

                <li className="book-btn-wrapper">
                    <Bone width={100} height={30}/>
                </li>

            </ul>
        </div>
    )
};

export const setBranchDaysSkeleton = () => {
    return (
        <div className="times-wrapper">
            <div>
                <Bone width={30}/>
            </div>
            <div className="times-skeleton" style={{display: "flex", flexWrap: "wrap"}}>
                <Bone width={15} height={70}/>
                <Bone width={15} height={70}/>
                <Bone width={15} height={70}/>
                <Bone width={15} height={70}/>
                <Bone width={15} height={70}/>
                <Bone width={15} height={70}/>
                <Bone width={15} height={70}/>
            </div>
        </div>
    )
};
export const setBranchTimesSkeleton = () => {
    return (
        <div className="times-wrapper">
            <div>
                <Bone width={30}/>
            </div>
            <div className="times-skeleton" style={{display: "flex", flexWrap: "wrap"}}>
                <Bone width={15} height={30}/>
                <Bone width={15} height={30}/>
                <Bone width={15} height={30}/>
                <Bone width={15} height={30}/>
                <Bone width={15} height={30}/>
                <Bone width={15} height={30}/>
                <Bone width={15} height={30}/>
            </div>
        </div>
    )
};
export const setPlanSkeleton = (count) => {
    let items = [];
    for (let i = 0; i < count; i++) {
        items.push(
            <div className="plan-skeleton-responsive"
                 style={{display: "flex", justifyContent: "space-between", width: "20%"}}>
                <Bone width={100} height={300} key={i}/>
            </div>
        )
    }
    return items;
};
export const setUserHistorySkeleton = (count) => {
    let items = [];
    for (let i = 0; i < count; i++) {
        items.push(
            <div className="col-md-6 col-xs-12 mb-30">
                <Bone width={100} height={200} key={i}/>
            </div>
        )
    }
    return items;
};
export const setUpcomingReservationSkeleton = (count) => {
    let items = [];
    for (let i = 0; i < count; i++) {
        items.push(
            <div className="mb-30" style={{width: "48%"}}>
                <Bone width={100} height={250} key={i}/>
            </div>
        )
    }
    return items;
};

export const setOfferCategorySkeleton = (count) => {
    let items = [];
    for (let i = 0; i < count; i++) {
        items.push(
            <Skull key={i}/>
        )
    }
    return items;
};
export const setHomeSliderSkeleton = () => {
    return (
        <Bone width={600} height={400}/>
    )
}
export const setHomeOutSliderSkeleton = () => {
    return (

        <section className="intro">
            <div className="container">
                <div className="row">
                    <div className="col-md-12">
                        <Bone width={100} height={400}/>
                    </div>
                </div>
            </div>
        </section>
    )
}

export const setAppointmentSkeleton = () => {
    return (
        <div className="branch">
            <div className="p-20">
                <Bone width={50}/>
                <Bone width={30}/>
                <div className="row margin-top-30">
                    <div className="col-4 offset-4 text-center">
                        <Skull/>
                    </div>
                </div>
                <Skeleton amount={3}/>
            </div>
        </div>
    )
};
export const setBreakTimesSkeleton = () => {
    return (
        <div style={{display: "flex"}}>
            <Bone width={30}/>
            <Bone width={30}/>
        </div>
    )
};
export const setShareIconsSkeleton = (count) => {
    let items = [];
    for (let i = 0; i < count; i++) {
        items.push(
            <Skull width={20} height={20}/>
        )
    }
    return items;
};
export const setBookWithOfferSkeleton = () => {
  return(
      <div className="offer-wrapper">
          <Bone width={30}/>
          <div>
              <Bone width={100} height={40}/>
          </div>
      </div>
  )
};
export const setHospitalProfileSkeleton = (count) => {
    let items = [];
    for (let i = 0; i < count; i++) {
        items.push(
            <Bone width={100} height={200}/>
        )
    }
    return items;
}