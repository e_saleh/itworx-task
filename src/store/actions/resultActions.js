import axios from "axios";
import {blogUrl, resultsApiLink} from "../../config";
import {types} from "./../types/globalTypes";
import {resultTypes} from "../types/resultTypes";
import parse from "parse-link-header";

export const getTypedResults = (type = null, page = null, limit = null, order = null, filter_name = null, filter_value = null, sort = null) => {

    return (dispatch, getState) => {
        dispatch({
            type: types.ERROR_500,
            payload: false,
        });

        axios.get(resultsApiLink + "?type=" + type + "&_page=" + page + "&_limit=" + limit + "&_order=" +
            order + "&" + filter_name + "=" + filter_value + "&_sort=" + sort).then(response => {
            let responseData = {
                "results": [],
            };
            if (response.data) {
                let responseData = {
                    "results": response.data,
                };
                const links = response.headers.link;
                if (links) {
                    const parsedLinks = parse(links);
                    const lastPage = {"last_page": parsedLinks.last._page}
                    responseData = Object.assign(responseData, lastPage);
                }

                dispatch({
                    type: resultTypes.GET_TYPED_RESULTS,
                    payload: responseData
                });
            }
        }).catch(error => {

            dispatch({
                type: types.ERROR_500,
                payload: true,
            });
        })

    };
};

export const getFormattedDateFiles = (type, dateType = null, date = null) => {

    return (dispatch, getState) => {
        dispatch({
            type: types.ERROR_500,
            payload: false,
        });

        axios.get(resultsApiLink + "?type=" + type).then(response => {
            let responseData = {};
            responseData = {
                "results": [],
            };
            if (response.data) {
                const fileDate = response.data.length ? response.data.map(file => {
                    let files = [];
                    const fileDate = new Date(file.created).getDate() + "-" + (new Date(file.created).getMonth() + 1) + "-" + new Date(file.created).getFullYear();
                    const fileYear = new Date(file.created).getFullYear();
                    switch (dateType) {
                        case "year":
                            const fileYear = new Date(file.created).getFullYear();
                            if (fileYear == date) {
                                files = [...files, file];
                                responseData = {
                                    "results": files,
                                };
                            }
                            break;
                        case "date":
                            let formatted_full_date = date.getDate() + "-" + (date.getMonth() + 1) + "-" + date.getFullYear();
                            console.log(formatted_full_date)
                            if (fileDate == formatted_full_date) {
                                files = [...files, file];
                                responseData = {
                                    "results": files,
                                };
                            }
                            break;
                        case "today":
                            let current_datetime = new Date();
                            let formatted_date = current_datetime.getDate() + "-" + (current_datetime.getMonth() + 1) + "-" + current_datetime.getFullYear()
                            if (fileDate === formatted_date) {
                                files = [...files, file];
                                responseData = {
                                    "results": files,
                                };
                            }
                            break;
                        default://nothing
                    }
                }) : null;

                const links = response.headers.link;
                if (links) {
                    const parsedLinks = parse(links);
                    const formattedDateLastPage = {"last_page": parsedLinks.last._page}
                    responseData = Object.assign(responseData, formattedDateLastPage);
                }
                dispatch({
                    type: resultTypes.GET_TYPED_RESULTS,
                    payload: responseData
                });
            }
        }).catch(error => {

            dispatch({
                type: types.ERROR_500,
                payload: true,
            });
        })

    };
};

export const getHomeData = (type, page, limit) => {
    return (dispatch, getState) => {
        dispatch({
            type: types.ERROR_500,
            payload: false,
        });

        axios.get(resultsApiLink + "?type=" + type + "&_page=" + page + "&_limit=" + limit).then(response => {
            if (response.data) {
                switch (type) {
                    case "people":
                        dispatch({
                            type: resultTypes.GET_PEOPLE_RESULTS,
                            payload: response.data,
                        });
                        break;
                    case  "apps":
                        dispatch({
                            type: resultTypes.GET_APPS_RESULTS,
                            payload: response.data,
                        });
                        break;
                    case "files":
                        dispatch({
                            type: resultTypes.GET_FILES_RESULTS,
                            payload: response.data,
                        });
                        break;
                    default:
                        return true;
                }
            }
        }).catch(error => {
            dispatch({
                type: types.ERROR_500,
                payload: true,
            });
        })

    };
};

export const clearTypedResultsData = () => {
    return (dispatch, getState) => {
        dispatch({
            type: resultTypes.CLEAR_TYPED_RESULTS_DATA,
            payload: []
        });
    }
};
export const clearResultsData = () => {
    return (dispatch, getState) => {
        dispatch({
            type: resultTypes.CLEAR_RESULTS_DATA,
            payload: []
        });
    }
};

