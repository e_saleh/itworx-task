export const Error500 = () => {
    return (dispatch) => {
        dispatch({
            type: "ERROR_500",
            payload: false,
        })
    }
};


export const clearError500 = () => {
    return (dispatch) => {
        dispatch({
            type: "CLEAR_ERROR_500",
            payload: false,
        })
    }
};

