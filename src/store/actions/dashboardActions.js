import axios from "axios";


import {dashboardTypes} from "../types/dashboardTypes";
import {types} from "../types/globalTypes";
import {feedbackApiLink} from "../../config";

export const getFeedback = (page, limit) => {
    return (dispatch, getState) => {
        dispatch({
            type: types.ERROR_500,
            payload: false,
        });

        axios.get(feedbackApiLink).then(response => {
            let responseData = {
                "results": response.data,
            };
            let feedbackYes = [];
            let feedbackNo= [];
            let noOfFeedbackYes, noOfFeedbackNo;
            if (response.data) {
                responseData = {
                    "results": response.data,
                };
             const statistics =  response.data.length ? response.data.map(feedback => {
                    if (feedback.answer === "yes") {
                        feedbackYes = [...feedbackYes,feedback];
                        const noOfFeedbackYes = {"noOfFeedbackYes": feedbackYes.length};
                        responseData= Object.assign(responseData, noOfFeedbackYes);
                        console.log(responseData);

                    }else {
                        feedbackNo = [...feedbackNo,feedback];
                        const noOfFeedbackNo = {"noOfFeedbackNo": feedbackNo.length};
                        responseData= Object.assign(responseData, noOfFeedbackNo);
                    }
                }) : null;
                dispatch({
                    type: dashboardTypes.GET_FEEDBACK,
                    payload: responseData
                });
            }
        }).catch(error => {

            dispatch({
                type: types.ERROR_500,
                payload: true,
            });
        })

    };
};
