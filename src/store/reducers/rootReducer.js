import {combineReducers} from 'redux'
import resultReducer from "./resultReducer";
import dashboardReducer from "./dashboardReducer";

const rootReducer = combineReducers({
    resultReducer: resultReducer,
    dashboardReducer: dashboardReducer
});

export default rootReducer


