import {dashboardTypes} from "./../types/dashboardTypes";

const initState = {
    feedbackResults: [],
    noOfFeedbackYes: 0,
    noOfFeedbackNo: 0,
};


const dashboardReducer = (state = initState, action) => {
    switch (action.type) {
        case dashboardTypes.GET_FEEDBACK:
            state = {

                ...state,
                feedbackResults: action.payload.results,
                noOfFeedbackYes: action.payload.noOfFeedbackYes,
                noOfFeedbackNo: action.payload.noOfFeedbackNo,
            };
            break;

        default:
        // do nothing
    }
    return state;
};


export default dashboardReducer;