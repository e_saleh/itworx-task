import {types} from "./../types/globalTypes";

const initState = {
    route: null,
    error500: false,
};


const globalReducer = (state = initState, action) => {
    switch (action.type) {
        case types.SET_CURRENT_ROUTE:
            state = {

                ...state,
                route: action.payload,
            };
            break;
        case types.ERROR_500:
            state = {
                ...state,
                error500: action.payload,
            };
            break;
        case types.CLEAR_ERROR_500:
            state = {
                ...state,
                error500: false,
            };
            break;
        default:
        // do nothing
    }
    return state;
};


export default globalReducer;