import {resultTypes} from "./../types/resultTypes";

const initState = {
    typedResults: [],
    typedResultsIsLoading: true,
    typedResultsIsEmpty: false,
    resultsIsEmpty: false,
    lastPage: null,
    peopleResults: [],
    filesResults: [],
    appsResults: [],
    homeResultsIsLoading: true,
};


const resultReducer = (state = initState, action) => {
    switch (action.type) {
        case resultTypes.GET_TYPED_RESULTS:
            state = {
                ...state,
                typedResults: [...state.typedResults, ...action.payload.results],
                typedResultsIsLoading: false,
                lastPage: action.payload.last_page
            };
            break;
        case resultTypes.GET_PEOPLE_RESULTS:
            state = {
                ...state,
                peopleResults: action.payload,
                homeResultsIsLoading: false,
            };
            break;
        case resultTypes.GET_APPS_RESULTS:
            state = {
                ...state,
                appsResults: action.payload,
                homeResultsIsLoading: false,
            };
            break;
        case resultTypes.GET_FILES_RESULTS:
            state = {
                ...state,
                filesResults: action.payload,
                homeResultsIsLoading: false,
            };
            break;
        case resultTypes.GET_TYPED_RESULTS_NOT_FOUND:
            state = {
                ...state,
                typedResultsIsEmpty: action.payload,
            };
            break;
        case resultTypes.GET_RESULTS_NOT_FOUND:
            state = {
                ...state,
                resultsIsEmpty: action.payload,
            };
            break;
        case resultTypes.CLEAR_TYPED_RESULTS_DATA:
            state = {
                ...state,
                typedResults: [],
                typedResultsIsLoading: true,
                typedResultsIsEmpty: false,
            };
            break;
        case resultTypes.CLEAR_RESULTS_DATA:
            state = {
                ...state,
                filesResults: [],
                peopleResults: [],
                appsResults: []
            };
            break;
        default:
        // do nothing
    }
    return state;
};


export default resultReducer;