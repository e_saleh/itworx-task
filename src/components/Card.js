import React, {Component} from 'react'
import ProgressiveImage from "react-progressive-image-loading";


class Card extends Component {
    render() {
        const {image, title, subTitle, info} = this.props;
        return (
            <div className="card-area">
                <div className="card gray">
                    <div className="card-body">

                        <div className="flex-m">
                            <ProgressiveImage
                                preview="images/grey.jpg"
                                src={image}
                                render={(src, style) => <img alt="avatar" src={src} className="img-avatar" style={style}/>}
                            />
                            <div className="flex-direction-col ml-20 card-text">
                                <h4 className="no-margin">{title}</h4>
                                <p className="no-margin grey">{subTitle}</p>
                                {info ? <p className="no-margin">{info}</p> : null}
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        );
    }
}

export default Card