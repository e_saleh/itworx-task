import React from "react";

const NoData = () => {
    return (
        <div className="card-area">
            <div className="card gray no-padding">
                <div className="card-body flex justify-content-center">
                    <p className="no-margin">No Results found!</p>
                </div>
            </div>
        </div>
    )
};

export default NoData