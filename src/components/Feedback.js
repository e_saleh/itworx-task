import React, {Component} from 'react'
import {Button, Modal} from 'react-bootstrap';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faSmileBeam} from '@fortawesome/free-regular-svg-icons'
import {faSmile} from '@fortawesome/free-regular-svg-icons'
import {faFrown} from '@fortawesome/free-regular-svg-icons'
import "./../css/globale.scss"
import axios from "axios"
import {feedbackApiLink, getHeader} from "../config";
import uuid from "uuid"
import {showToast} from "../helpers/magic";

class Feedback extends Component {

    state = {
        feedbackMsg: "",
        answer: "",
        answerYesColor: "black",
        answerNoColor: "black",
        feedbackMsgValidation: "",
    };
    handleInputChange = (e) => {
        if ((e.target.value).length >= 500) {
            this.setState({feedbackMsgValidation: "Sorry, Message cannot contain more than 500 characters!"})
        }else {
            this.setState({feedbackMsgValidation: ""})
        }
        this.setState({feedbackMsg: e.target.value})
    };
    handleAnswerChange = (e) => {
        e.preventDefault();
        if (e.currentTarget.name === "yes") {
            this.setState({
                answer: "yes",
                answerYesColor: "green",
                answerNoColor: "black"
            })
        } else if (e.currentTarget.name === "no") {
            this.setState({
                answer: "no",
                answerYesColor: "black",
                answerNoColor: "red"
            })
        }
    };
    handleSubmission = (e) => {
        e.preventDefault();

        axios.post(feedbackApiLink, {
            "id": uuid.v4(),
            "feedback": this.state.feedbackMsg,
            "answer": this.state.answer
        }, {headers: getHeader()}).then(response => {
            if (response.status === 201) {
                showToast("Feedback sent successfully", "green");
                this.props.close();
            }
        }).catch(error => {
            if (error) {
                showToast("Ooops, Something went wrong", "red");
            }
        })
    };

    render() {

        return (
            <div>

                <Modal show={this.props.showModal} onHide={this.props.close}>
                    <Modal.Header closeButton>
                        <Modal.Title>
                            <FontAwesomeIcon icon={faSmileBeam} color="black" className="mr-20"/>
                            Send Feedback
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <h4 className="text-center">How do you rate your experience so far ?</h4>
                        <div className="text-center mb-30">
                            <button className="btn-style-less" name="yes" onClick={this.handleAnswerChange}>
                                <FontAwesomeIcon icon={faSmile} color={this.state.answerYesColor}
                                                 className="mr-20 cursor-pointer"
                                                 size="3x"/></button>
                            <button className="btn-style-less" name="no" onClick={this.handleAnswerChange}>
                                <FontAwesomeIcon icon={faFrown} color={this.state.answerNoColor}
                                                 className="mr-20 cursor-pointer"
                                                 size="3x"/></button>
                        </div>
                        <h4 className="text-center">What would you like to share with us ? (optional)</h4>
                        <textarea rows="5" maxLength="500" className="full-width" name="feedbackMsg"
                                  placeholder="Write your feedback here" onChange={this.handleInputChange}
                        />
                        <small className="red">{this.state.feedbackMsgValidation}</small>
                        <div className="text-center mt-20 full-width">
                            <Button className="btn" onClick={this.handleSubmission}>Send Feedback</Button>
                        </div>
                    </Modal.Body>
                </Modal>
            </div>
        )
    }
}

export default Feedback