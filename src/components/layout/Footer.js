import React from "react";
import "./../../css/layout/footer.scss"


const Footer = () => {
    return (
        <footer id="footer">
            <div className="container">
                <h6>© 2019 ITWorx All Rights Reserved </h6>
            </div>
        </footer>
    )
};

export default Footer