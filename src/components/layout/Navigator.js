import React, {Component, Fragment} from "react"
import {NavLink, withRouter} from "react-router-dom";
import "./../../css/layout/navigator.scss"
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faTachometerAlt} from '@fortawesome/free-solid-svg-icons'
import {faSmileBeam} from '@fortawesome/free-solid-svg-icons'
import Feedback from "../Feedback";

class Navigator extends Component {
    state = {
        showModal: false
    };
    close = () => {
        this.setState({showModal: false});
    };

    open = () => {
        this.setState({showModal: true});
    };
    handleKeyDown = (e) => {
        if (e.key === 'Enter') {
            e.preventDefault();
            this.props.history.push('/all');
        }
    };

    render() {
        return (
            <Fragment>
                <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
                    {
                        this.props.isSelect ?
                            <form className="form-inline my-2 my-lg-0">
                                <input className="form-control mr-sm-2" type="search" placeholder="Search"
                                       aria-label="Search"
                                       onKeyDown={this.handleKeyDown}/>
                            </form> : null
                    }

                    <button className="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                            aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>

                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul className="navbar-nav ml-auto">
                            <li className="nav-item">
                                <NavLink className="nav-link cursor-pointer" to="/all">
                                    Home
                                </NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink className="nav-link cursor-pointer" to="/people">
                                    People
                                </NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink className="nav-link cursor-pointer" to="/files">
                                    Files
                                </NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink className="nav-link cursor-pointer" to="/apps">
                                    Apps </NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink className="nav-link cursor-pointer" to="/dashboard">
                                    <FontAwesomeIcon icon={faTachometerAlt} size="2x"/>
                                </NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink className="nav-link cursor-pointer" to="#" onClick={this.onOpenModal}>
                                    <FontAwesomeIcon icon={faSmileBeam} size="2x"  bsStyle="primary"
                                                     bsSize="large"
                                                     onClick={this.open} color="white"/>
                                    <Feedback showModal={this.state.showModal} close={this.close}/>

                                </NavLink>
                            </li>

                        </ul>

                    </div>
                </nav>
            </Fragment>
        );
    }
}

export default withRouter(Navigator)