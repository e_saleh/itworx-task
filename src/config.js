import './../node_modules/izitoast/dist/css/iziToast.min.css';


export const baseLink = "http://localhost:3001/";

export const usersApiLink = baseLink + "users/";
export const feedbackApiLink = baseLink + "feedback/";
export const resultsApiLink = baseLink + "results/";


export const getHeader = () => {
    const header = {
        'Content-Type': 'application/json'
    };
    return header
};
