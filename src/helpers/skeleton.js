import {Bone, Skeleton, Skull} from "react-loading-skeleton-placeholders";
import React from "react";

export const setCardSkeleton = (count) => {
    let items = [];
    for (let i = 0; i < count; i++) {
        items.push(
            <div className="card-area">
                <Bone width={100} height={150}/>
            </div>
        )
    }
    return (
        <div className="results-container">
            <div className="results">
                {items}
            </div>
        </div>
    );
};

export const setHomeSkeleton = (count) => {

    let items = [];
    for (let i = 0; i < count; i++) {
        items.push(
            <div className="card-area">
                <Bone width={100} height={400}/>
            </div>
        )
    }
    return (
        <div className="results-container">
            <div className="home-results">
                {items}
            </div>
        </div>
    );
};