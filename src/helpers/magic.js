import izitoast from "izitoast";
import './../../node_modules/izitoast/dist/css/iziToast.min.css';

export const slice_text = (text, length) => {
    if (text.length >= length) {
        return text.slice(0, length) + " ..."
    } else {
        return text
    }
};

export const timeFormat = (fullDate) =>  {
    const months = ["JANUARY", "FEBRUARY", "MARCH", "APRIL", "MAY", "JUNE", "JULY", "AUGUST", "SEPTEMBER", "OCTOBER", "NOVEMBER", "DECEMBER"];
    let file_date = new Date(fullDate);
    const date = file_date.getDate() + "-" + months[file_date.getMonth()] + "-" + file_date.getFullYear()
        + " " + "at" + " " + file_date.getHours() + ":" + file_date.getMinutes() + ":" + file_date.getSeconds();
    return date;
};
export const isToday = (fullDate) => {
    const today = new Date();
    if (fullDate.getDate() == today.getDate() &&
        fullDate.getMonth() == today.getMonth() &&
        fullDate.getFullYear() == today.getFullYear())
    {
        return true
    }
    return false
};
export const isYear = (fullDate) => {
    const today = new Date();
    if (fullDate.getFullYear() == today.getFullYear())
    {
        return true
    }
    return false
};

export const shorten_text = (str, maxLength) => {
    let cut= str.indexOf(' ', maxLength);
    if(cut === -1) return str ;
    cut  = str.substring(0, cut) + " ...";
    return cut;
};

export const trimString = (str, length) => {
    return (
        str.substr(0, length)
    );
};
export const stripHtml = (html) => {
    // Create a new div element
    var temporalDivElement = document.createElement("div");
    // Set the HTML content with the providen
    temporalDivElement.innerHTML = html;
    // Retrieve the text property of the element (cross-browser support)
    return temporalDivElement.textContent || temporalDivElement.innerText || "";
};
export const splitStr = (str, splitWith) => {
    return (
        str.split(splitWith)
    );
};
export const showToast = (msg, color) => {
    izitoast.show({
        color: color,
        title: "",
        message: msg,
        imageWidth: 50,
        layout: 1,
        balloon: false,
        close: true,
        rtl: false,
        position: 'bottomRight', // bottomRight, bottomLeft, topRight, topLeft, topCenter, bottomCenter, center
        target: '',
        timeout: 5000,
        pauseOnHover: true,
        resetOnHover: false,
        progressBar: true,
        animateInside: true,
        transitionIn: 'fadeInUp',
        transitionOut: 'fadeOut',
        transitionInMobile: 'fadeInUp',
        transitionOutMobile: 'fadeOutDown',

    });
};