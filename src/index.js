import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import * as serviceWorker from './serviceWorker';
import {createStore, applyMiddleware} from "redux";
import {Provider} from "react-redux"
import rootReducer from "./store/reducers/rootReducer";
import thunk from "redux-thunk" // allow asynchronous actions
import logger from "redux-logger";

// Here is list of middlewares  applied in development...
const applied_middleware = applyMiddleware(logger,  thunk);

const store = createStore(
    rootReducer,
    applied_middleware,
);

ReactDOM.render(
    <Provider store={store}>
            <App />
    </Provider>, document.getElementById('root'));

serviceWorker.unregister();

