import React from "react"
import {Route, Switch} from "react-router-dom";
import Master from "./views/layout/Master";
import Home from "./views/pages/home/Home";
import Welcome from "./views/pages/Welcome";
import People from "./views/pages/people/People";
import Files from "./views/pages/files/Files";
import Apps from "./views/pages/apps/Apps";
import Login from "./views/pages/Login";
import Dashboard from "./views/pages/Dashboard";

class Routes extends React.Component {

    render() {
        const Layout = ({component: Component, ...rest}) => (
            <Route {...rest} render={(props) => (
                <Master isSelect={rest.isSelect} >
                    <Component title={rest.title} isSelect={rest.isSelect}  {...props} />
                </Master>
            )}/>
        );
        return (
            <Switch>
                <Layout exact path="/" title="Welcome" isSelect={false} component={Welcome}/>
                <Layout exact path="/all" title="Home" isSelect={true} component={Home}/>
                <Layout exact path="/people" title="People" isSelect={true} component={People}/>
                <Layout exact path="/files" title="Files" isSelect={true} component={Files}/>
                <Layout exact path="/apps" title="Apps" isSelect={true} component={Apps}/>
                <Layout exact path="/login" title="Login" isSelect={true} component={Login}/>
                <Layout exact path="/dashboard" title="Dashboard" isSelect={true} component={Dashboard}/>

            </Switch>
        );
    }
}

export default Routes;